import Image from "next/image";
import React from "react";
import RedditFace from "../assets/RedditFace.svg";
import RedditText from "../assets/RedditText.svg";
import SearchInput from "./SearchInput";
import RightContent from "./RightContent";

const NavBar = () => {
  return (
    <header className="flex h-12 items-center px-5">
      <div className="relative h-10 w-20 flex">
        <Image src={RedditFace} alt="RedditFace" />
        <Image src={RedditText} alt="RedditText" />
      </div>
      <div className="flex-1 max-w-[690px] my-0 mx-auto">
        <SearchInput />
      </div>
      <div>
        <RightContent />
      </div>
    </header>
  );
};

export default NavBar;
