import React from "react";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { UserIcon } from "@heroicons/react/outline";

const RightContent = () => {
  return (
    <div>
      <div className="flex items-center">
        <div className="flex items-center">
          <button className="text-[#0079D3] border-[1px] border-[#0079D3] border-solid w-[120px] ml-4 min-h-[32px] min-w-[32px] rounded-full text-[14px] font-bold">
            Get App
          </button>
          <button className="bg-[#0079D3] text-[#FFFFFF] rounded-full min-h-[32px] min-w-[32px] w-[120px] leading-[17px] ml-4">
            Log In
          </button>
        </div>
        <button className="flex items-center">
          <UserIcon className="h-8 text-gray-400" />
          <ChevronDownIcon className="h-8 text-gray-400" />
        </button>
      </div>
    </div>
  );
};

export default RightContent;
