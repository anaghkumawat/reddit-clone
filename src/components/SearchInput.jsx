import React from "react";
import { SearchIcon } from "@heroicons/react/solid";

const SearchInput = () => {
  return (
    <div className="mx-16 w-auto h-auto">
      <form className="flex items-center space-x-2 bg-[#F6F7F8] rounded-full border-gray-200 px-3 py-1">
        <SearchIcon className="h-6 w-6 text-gray-400 rounded-l-full" />
        <input
          type="text"
          placeholder="Search Reddit"
          className="outline-none bg-transparent flex-1 text-[#1c1c1c] text-[14px] leading-[14px] mr-[16px] w-full font-normal rounded-r-full"
        />
        <button type="submit" hidden></button>
      </form>
    </div>
  );
};

export default SearchInput;
